package com.yasiuraroman.model;

public interface Card {
    void processOrder(Order order);
}
