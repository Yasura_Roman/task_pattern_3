package com.yasiuraroman.model;

import java.util.List;
import java.util.stream.Collectors;

public class UsualBouquet implements Bouquet{
    private String name;
    private EventType eventType;
    private List<Flower> flowers;

    public UsualBouquet() {
    }

    public UsualBouquet(String name, EventType eventType, List<Flower> flowers) {
        this.name = name;
        this.eventType = eventType;
        this.flowers = flowers;
    }

    @Override
    public Integer getPrice() {
        return flowers.stream()
                .mapToInt(e -> e.getPrice())
                .sum();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public EventType getEventType() {
        return eventType;
    }

    @Override
    public String getBouquetComponents() {
        return flowers.stream()
                .map(e -> e.getName())
                .collect(Collectors.joining("  "));
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    public List<Flower> getFlowers() {
        return flowers;
    }

    public void setFlowers(List<Flower> flowers) {
        this.flowers = flowers;
    }
}
