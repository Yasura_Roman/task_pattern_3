package com.yasiuraroman.model;

public interface Bouquet {
    String getName();
    EventType getEventType();
    String getBouquetComponents();
    Integer getPrice();
}
