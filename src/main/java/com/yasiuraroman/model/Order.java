package com.yasiuraroman.model;

import java.util.Optional;

public class Order {
    private Bouquet bouquet;
    private Card card;
    Optional<Delivery> delivery = Optional.empty();
    Integer price;

    public Order() {
    }

    public Bouquet getBouquet() {
        return bouquet;
    }

    public void setBouquet(Bouquet bouquet) {
        this.bouquet = bouquet;
        price = bouquet.getPrice();
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public Optional<Delivery> getDelivery() {
        return delivery;
    }

    public void setDelivery(Optional<Delivery> delivery) {
        this.delivery = delivery;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public int getTotalPrise(){
        if (card != null){
            card.processOrder(this);
        }
        return price + delivery.orElse(new Delivery(0,"")).getPrice();
    }

    public String getItems(){
        if(delivery.isPresent()){
            return bouquet.getBouquetComponents() + "delivery to "+delivery.get().getAddress();
        }
        return bouquet.getBouquetComponents();
    }
}
