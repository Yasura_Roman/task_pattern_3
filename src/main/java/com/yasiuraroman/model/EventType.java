package com.yasiuraroman.model;

public enum EventType {
    CLASSIC,        //простий
    WEDDING,        //весілля
    BIRTHDAY,       //день народження
    FUNERAL,        //похорон
    VALENTINES;     //валентина
}
