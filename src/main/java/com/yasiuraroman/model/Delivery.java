package com.yasiuraroman.model;

public class Delivery {

    private Integer price;
    private String address;

    public Delivery(Integer price, String address) {
        this.price = price;
        this.address = address;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
