package com.yasiuraroman.model.lists;

import com.yasiuraroman.model.Order;

import java.util.ArrayList;
import java.util.List;

public class OrderList {
    private static OrderList instance = null;
    private List<Order> orderList;

    private OrderList(){
        orderList = new ArrayList<>();
    }

    public static OrderList getInstance() {
        if(instance == null){
            synchronized (OrderList.class){
                if(instance == null){
                    instance = new OrderList();
                }
            }
        }
        return instance;
    }

    public void addOrder(Order order){
        orderList.add(order);
    }

    public List<Order> getOrderList() {
        return orderList;
    }
}
