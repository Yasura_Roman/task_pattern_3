package com.yasiuraroman.model.lists;

import com.yasiuraroman.model.Bouquet;
import com.yasiuraroman.model.EventType;
import com.yasiuraroman.model.Flower;
import com.yasiuraroman.model.UsualBouquet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BouquetList {

    private static BouquetList instance = null;
    private List<Bouquet> bouquetList;

    private BouquetList(){
        initialization();
    }

    public static BouquetList getInstance() {
        if(instance == null){
            synchronized (BouquetList.class){
                if(instance == null){
                instance = new BouquetList();
                }
            }
        }
        return instance;
    }

    private void initialization(){
        bouquetList = new ArrayList<>();
        List<Flower> flowers;

        flowers = Arrays.asList(new Flower[] {
                new Flower("Roses",20,"red"),
                new Flower("Roses",20,"red"),
                new Flower("Roses",20,"red"),
        });
        bouquetList.add(new UsualBouquet("3roses", EventType.CLASSIC,flowers ));

        flowers = Arrays.asList(new Flower[] {
                new Flower("Roses",20,"red"),
                new Flower("Roses",20,"red"),
                new Flower("Roses",20,"red"),
                new Flower("Roses",20,"red"),
                new Flower("Roses",20,"red"),
        });
        bouquetList.add(new UsualBouquet("5roses", EventType.CLASSIC,flowers ));

        flowers = Arrays.asList(new Flower[] {
                new Flower("Roses",20,"white"),
                new Flower("Roses",20,"white"),
                new Flower("Roses",20,"white"),
        });
        bouquetList.add(new UsualBouquet("3roseswhite", EventType.CLASSIC,flowers ));
    }

    public List<Bouquet> getBouquetList() {
        return bouquetList;
    }
}
