package com.yasiuraroman.model.decorator;

import com.yasiuraroman.model.Bouquet;
import com.yasiuraroman.model.BouquetDecorator;
import com.yasiuraroman.model.EventType;

public class BasketBouquet extends BouquetDecorator {

    private static final Integer PRICE = 75;

    public BasketBouquet(Bouquet bouquet) {
        setBouquet(bouquet);
    }

    @Override
    public String getName() {
        return bouquet.getName() + " in basket";
    }

    @Override
    public EventType getEventType() {
        return bouquet.getEventType();
    }

    @Override
    public String getBouquetComponents() {
        return bouquet.getBouquetComponents() + " basket";
    }

    @Override
    public Integer getPrice() {
        return bouquet.getPrice() + PRICE;
    }
}
