package com.yasiuraroman.model;

public abstract class BouquetDecorator implements Bouquet{
    protected Bouquet bouquet;

    public BouquetDecorator setBouquet(Bouquet bouquet) {
        this.bouquet = bouquet;
        return this;
    }

}
