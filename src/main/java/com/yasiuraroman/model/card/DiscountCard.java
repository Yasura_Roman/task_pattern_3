package com.yasiuraroman.model.card;

import com.yasiuraroman.model.Card;
import com.yasiuraroman.model.Order;

public class DiscountCard implements Card {

    @Override
    public void processOrder(Order order) {
        order.setPrice((int)(order.getPrice() * 0.5));
    }
}
