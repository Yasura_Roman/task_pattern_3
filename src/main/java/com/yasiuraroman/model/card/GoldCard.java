package com.yasiuraroman.model.card;

import com.yasiuraroman.model.Card;
import com.yasiuraroman.model.Order;

public class GoldCard implements Card {
    @Override
    public void processOrder(Order order) {
        if(order.getDelivery().isPresent()){
            order.getDelivery().get().setPrice(0);
        }
    }
}
