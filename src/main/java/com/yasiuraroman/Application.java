package com.yasiuraroman;

import com.yasiuraroman.model.Order;
import com.yasiuraroman.view.OrderView;

public class Application {
    public static void main(String[] args) {
        OrderView orderView = new OrderView();
        orderView.show();
    }
}
