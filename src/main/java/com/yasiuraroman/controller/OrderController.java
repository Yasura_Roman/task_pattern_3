package com.yasiuraroman.controller;

import com.yasiuraroman.model.Bouquet;
import com.yasiuraroman.model.Order;

import java.util.List;

public interface OrderController {
    List<Bouquet> getBouquetList();

    void setBouquet(Bouquet bouquet);

    List<String> getPackingList();

    void setPacking(String s);

    void addDelivery(String address);

    String getDeliveryPlace();

    List<String> getCardList();

    String setCard(String s);

    String showContent();

    Order submit();

    void newOrder();

    List<Order> showOrderList();
}

