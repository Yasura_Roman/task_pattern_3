package com.yasiuraroman.controller;

import com.yasiuraroman.model.Bouquet;
import com.yasiuraroman.model.BouquetDecorator;
import com.yasiuraroman.model.Delivery;
import com.yasiuraroman.model.Order;
import com.yasiuraroman.model.card.DiscountCard;
import com.yasiuraroman.model.card.GoldCard;
import com.yasiuraroman.model.decorator.BasketBouquet;
import com.yasiuraroman.model.decorator.PaperBouquet;
import com.yasiuraroman.model.decorator.TapeBouquet;
import com.yasiuraroman.model.lists.BouquetList;
import com.yasiuraroman.model.lists.OrderList;

import java.util.ArrayList;
import java.util.List;

public class OrderControllerImpl implements OrderController {

    private Order order ;

    public OrderControllerImpl() {
        order = new Order();
    }

    @Override
    public List<Bouquet> getBouquetList() {
        return BouquetList.getInstance().getBouquetList();
    }

    @Override
    public void setBouquet(Bouquet bouquet) {
        order.setBouquet(bouquet);
    }

    @Override
    public List<String> getPackingList() {
        List<String> list = new ArrayList<>();
        list.add("non");
        list.add("basket");
        list.add("paper");
        list.add("tape");
        return list;
    }

    @Override
    public void setPacking(String s) {
        //TODO Enum для вибору типу букету
        if (order.getBouquet() == null){
            return;
        }
        BouquetDecorator decorator = null;
        switch (s){
            case "basket": {
                decorator = new BasketBouquet(order.getBouquet());
                break;
            }
            case "paper": {
                decorator = new PaperBouquet(order.getBouquet());
                break;
            }
            case "tape" : {
                decorator = new TapeBouquet(order.getBouquet());
                break;
            }
            default:break;
        }
        if (decorator != null){
            order.setBouquet(decorator);
        }
    }

    @Override
    public void addDelivery(String address) {
        order.setDelivery(java.util.Optional.of(new Delivery(50, address)));
    }

    @Override
    public String getDeliveryPlace() {
        if(order.getDelivery().isPresent()){
            return order.getDelivery().get().getAddress();
        }
        return "delivery is not exist";
    }

    @Override
    public List<String> getCardList() {
        List<String> list = new ArrayList<>();
        list.add("discount");
        list.add("gold");
        return list;
    }

    @Override
    public String setCard(String s) {
        String result = "card not changed";
        //TODO Enum для вибору типу card
        switch (s){
            case "discount": {
                order.setCard(new DiscountCard());
                result = "card is discount";
                break;
            }
            case "gold": {
                order.setCard(new GoldCard());
                result = "card is gold";
                break;
            }
            default:break;
        }
        return result;
    }

    @Override
    public String showContent() {
        return order.getItems();
    }

    @Override
    public Order submit() {
        OrderList.getInstance().addOrder(order);
        return order;
    }

    @Override
    public void newOrder() {
        order = new Order();
    }

    @Override
    public List<Order> showOrderList() {
        return OrderList.getInstance().getOrderList();
    }

}
