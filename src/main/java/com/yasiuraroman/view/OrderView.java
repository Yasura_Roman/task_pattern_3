package com.yasiuraroman.view;

import com.yasiuraroman.controller.OrderController;
import com.yasiuraroman.controller.OrderControllerImpl;
import com.yasiuraroman.model.Bouquet;
import com.yasiuraroman.model.Order;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class OrderView {
    private Logger logger = LogManager.getLogger();
    private Scanner input = new Scanner(System.in);
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private OrderController controller;
    private ResourceBundle menuBundle = ResourceBundle.getBundle("Menu");

    public OrderView() {
        //TODO refactor
        controller = new OrderControllerImpl();
        createMenu();
        initMenuCommands();
    }

    private void createMenu(){
        menu = new LinkedHashMap<>();
        SortedSet<String> sortedSet = new TreeSet<>();
        sortedSet.addAll(menuBundle.keySet());
        for (String key : sortedSet
        ) {
            menu.put(key,menuBundle.getString(key));
        }
    }

    //-------------------------------------------------------------------------
    private void initMenuCommands() {
        methodsMenu = new HashMap<>();
        methodsMenu.put("1", this::setBouquet);
        methodsMenu.put("2", this::setPacking);
        methodsMenu.put("3", this::setDelivery);
        methodsMenu.put("4", this::setCard);
        methodsMenu.put("5", this::showContent);
        methodsMenu.put("6", this::submit);
        methodsMenu.put("7", this::newOrder);
        methodsMenu.put("8", this::showOrderList);
    }

    private void setBouquet() {
        String userInput;
        for (Bouquet bouquet:controller.getBouquetList()
        ) {
            logger.info(bouquet.getName());
        }

        userInput = input.nextLine();
        for (Bouquet bouquet:controller.getBouquetList()
        ) {
            if(userInput.equals(bouquet.getName())){
                controller.setBouquet(bouquet);
                logger.info(bouquet.getName() + " is selected");
                return;
            }
        }
        logger.info("bouquet is not changed");

    }

    private void setPacking() {
        String userInput;
        for (String s:controller.getPackingList()
        ) {
            logger.info(s);
        }

        userInput = input.nextLine();
        for (String s:controller.getPackingList()
        ) {
            if(userInput.equals(s)){
                controller.setPacking(s.toLowerCase());
                logger.info(s + " is selected");
                return;
            }
        }
        logger.info("bouquet is not changed");
    }

    private void setDelivery() {
        logger.info("set address");
        controller.addDelivery(input.nextLine());
        logger.info(controller.getDeliveryPlace());
    }

    private void setCard() {
        String userInput;
        for (String s:controller.getCardList()
        ) {
            logger.info(s);
        }

        userInput = input.nextLine();
        for (String s:controller.getCardList()
        ) {
            if(userInput.equals(s)){
                controller.setCard(s.toLowerCase());
                logger.info(s + " is selected");
                return;
            }
        }
    }

    private void showContent() {
        logger.info(controller.showContent());
    }

    private void submit() {
        logger.info("total price = " + controller.submit().getTotalPrise());
    }

    private void newOrder(){
        controller.newOrder();
        logger.info("create new order");
    }

    private void showOrderList(){
        controller.showOrderList().stream().forEach( e -> logger.info(e.getItems()
                + "\n" + e.getTotalPrise() ));
    }

    //-------------------------------------------------------------------------

    private void outputMenu() {
        logger.info("ORDER MENU:");
        for (String str : menu.values()) {
            logger.info(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            logger.info("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } while (!keyMenu.equals("Q"));
    }
}
