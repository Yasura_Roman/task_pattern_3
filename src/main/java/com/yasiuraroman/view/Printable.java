package com.yasiuraroman.view;
@FunctionalInterface
public interface Printable {
    void print();
}
